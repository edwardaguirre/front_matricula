import React, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'reactstrap';
// import './App.css';

class BoletaPagoMatricula extends Component {

    constructor() {
        super();
        this.state = {
            student: []
        }
    }

    componentWillMount() {
        const { dni_usuario } = this.props.match.params

        axios.get(`http://127.0.0.1:3000/api/matricula/ver-detalle/${dni_usuario}`).then(({ data }) => {

            this.setState({
                student: data.data[0]
            });

        });
    }

    render() {

        const { nombre, apellido, dni_usuario, nombre_carrera } = this.state.student;

        return (
            <div className="App container">
                <div>
                    <h2>Boleta de Pago</h2>
                </div>
                <div className="detalle-user">
                    <Table>
                        <tbody>
                            <tr>
                                <th>#</th>
                                <td>00000111</td>
                            </tr>
                            <tr>
                                <th>DNI</th>
                                <td>{dni_usuario}</td>
                            </tr>
                            <tr>
                                <th>Nombre</th>
                                <td>{nombre}</td>
                            </tr>
                            <tr>
                                <th>Apellido</th>
                                <td>{apellido}</td>
                            </tr>
                            <tr>
                                <th>carrera</th>
                                <td>{nombre_carrera}</td>
                            </tr>
                            <tr>
                                <th>Total</th>
                                <td>$0000000</td>
                            </tr>
                        </tbody>
                    </Table>
                    <Button onClick={() => window.print()}>PRINT</Button>
                </div>
            </div >
        );
    }
}

export default BoletaPagoMatricula;

import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { postMatricula } from './utils/api';
import axios from 'axios';
import { FormGroup, Form, Input, Label, Button } from 'reactstrap';

// import './App.css';

class DetalleEstudiante extends Component {

    constructor() {
        super();
        this.state = {
            student: [],
            cSelected: [],
            id_carrera: 1,
            redirect: false
        }

        this.onCheckboxBtnClick = this.onCheckboxBtnClick.bind(this);
    }

    componentWillMount() {
        const { dni_usuario } = this.props.match.params

        axios.get(`http://127.0.0.1:3000/api/usuario/ver-detalle/${dni_usuario}`).then(({ data }) => {

            this.setState({
                student: data.data[0]
            });

        });
    }

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value,
        })
    }

    onCheckboxBtnClick(selected) {
        const index = this.state.cSelected.indexOf(selected);
        if (index < 0) {
            this.state.cSelected.push(selected);
        } else {
            this.state.cSelected.splice(index, 1);
        }
        this.setState({ cSelected: [...this.state.cSelected] });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);

        postMatricula(this.state)
            .then((res) => this.setState({ redirect: true }))
            .catch((err) => console.log(err));
    }

    render() {

        const { student: { nombre, apellido, dni }, redirect } = this.state;

        if (redirect) {
            return <Redirect to={`/boleta-pago-matricula/${dni}`} />;
        }

        return (
            <div className="App container">
                <div>
                    <h2>Detalle del estudiante</h2>
                </div>
                <div className="detalle-user">
                    <li><b>Nombre</b>: {nombre}</li>
                    <li><b>Apellido</b>: {apellido}</li>
                    <li><b>DNI</b>: {dni}</li>
                    <br />
                    <h3>Realiza matricula de materias</h3>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <Label htmlFor="nombre">Selecciona la carrera deseada</Label>
                            <Input type="select" name="id_carrera" onChange={this.handleChange.bind(this)} className="form-group" required>
                                <option disabled>Select</option>
                                <option value="1">Informatica</option>
                            </Input>
                        </FormGroup>
                        <FormGroup check>
                            <Input type="checkbox" onClick={() => this.onCheckboxBtnClick(1)} /> Fisica
                        </FormGroup>
                        <FormGroup check>
                            <Input type="checkbox" onClick={() => this.onCheckboxBtnClick(2)} /> Estadistica I
                        </FormGroup>
                        <FormGroup check>
                            <Input type="checkbox" onClick={() => this.onCheckboxBtnClick(3)} /> Estadistica II
                        </FormGroup>
                        <Button color="primary">Envíar</Button>
                    </Form>
                </div>
            </div >
        );
    }
}

export default DetalleEstudiante;

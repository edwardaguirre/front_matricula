import React, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'reactstrap'
// import './App.css';

class EstudiantesMatriculados extends Component {

    constructor() {
        super();
        this.state = {
            students: []
        }
    }

    componentWillMount() {
        axios.get('http://127.0.0.1:3000/api/matricula/list-estudiantes-matriculados').then(({ data }) => {

            this.setState({
                students: data.data
            });

        });
    }

    render() {
        let students = this.state.students.map((student, i) => {
            return (
                <tr key={i}>
                    <td>{i + 1}</td>
                    <td>{student.dni_usuario}</td>
                    <td>{student.nombre}</td>
                    <td>{student.apellido}</td>
                    <td>{student.nombre_carrera}</td>
                    <td>{student.estado}</td>
                    <td>
                        <a href={`/detalle-user/${student.dni_usuario}`} title="Ver detalle del estudiante">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                </tr>
            );
        })

        return (
            <div className="App container">
                <div>
                    <h2>Lista de estudiantes Matriculados</h2>
                    <Button onClick={() => window.print()}>PRINT</Button>
                </div>
                <div>
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>DNI</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>carrera</th>
                                <th>estado</th>
                                <th colSpan="2">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {students}
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }
}

export default EstudiantesMatriculados;

import React, { Component } from 'react';
import axios from 'axios';
import { Table, Button } from 'reactstrap'
// import './App.css';

class EstudiantesPendientes extends Component {

    constructor() {
        super();
        this.state = {
            students: []
        }
    }

    componentWillMount() {
        axios.get('http://127.0.0.1:3000/api/matricula/list-estudiantes-pendientes').then(({ data }) => {

            this.setState({
                students: data.data
            });

        });
    }

    onChangeStateMatricula(id_matricula) {
        axios.get(`http://127.0.0.1:3000/api/matricula/aceptar-matricula/${id_matricula}`).then(({ data }) => {

            // this.setState({
            //     students: data.data
            // });

            let elem = document.getElementById(`item_${id_matricula}`);
            elem.remove();
        });
    }

    render() {
        let students = this.state.students.map((student, i) => {
            return (
                <tr key={i} id={`item_${student.id_matricula}`}>
                    <td>{i + 1}</td>
                    <td>{student.dni_usuario}</td>
                    <td>{student.nombre}</td>
                    <td>{student.apellido}</td>
                    <td>{student.nombre_carrera}</td>
                    <td>{student.estado}</td>
                    <td>
                        <a href={`/detalle-user/${student.dni_usuario}`} title="Ver detalle del estudiante">
                            <i className="fa fa-eye"></i>
                        </a>
                    </td>
                    <td>
                        <a onClick={() => this.onChangeStateMatricula(student.id_matricula)} style={{ cursor: "pointer" }} title="Aprobar matricula">
                            <i className="fa fa-thumbs-up"></i>
                        </a>
                    </td>
                    <td>
                        <a href={`/boleta-pago-matricula/${student.dni_usuario}`} title="Ver boleta de pago">
                            <i className="fa fa-eye"></i>
                        </a>
                    </td>
                </tr>
            );
        })

        return (
            <div className="App container">
                <div>
                    <h2>Lista de estudiantes pendientes</h2>
                    <Button onClick={() => window.print()}>PRINT</Button>
                </div>
                <div>
                    <Table>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>DNI</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>carrera</th>
                                <th>estado</th>
                                <th colSpan="3">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {students}
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }
}

export default EstudiantesPendientes;

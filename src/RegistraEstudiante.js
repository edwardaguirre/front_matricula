import React, { Component } from 'react';
import { postStudent } from './utils/api';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Redirect } from 'react-router-dom';
// import './App.css';

class EstudiantesPendientes extends Component {

    constructor() {
        super();

        this.state = {
            nombre: '',
            apellido: '',
            dni: '',
            id_carrera: 1,
            redirect: false
        }
    }

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value,
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();

        postStudent(this.state)
            .then((res) => this.setState({ redirect: true }))
            .catch((err) => console.log(err));
    }


    render() {
        const { nombre, apellido, dni, redirect } = this.state;

        if (redirect) {
            return <Redirect to={`/detalle-user/${dni}`} />;
        }

        return (
            <div className="App container">
                <div>
                    <h2>Registro de estudiantes</h2>
                </div>
                <div>
                    <Form onSubmit={this.handleSubmit}>
                        <FormGroup>
                            <Label htmlFor="nombre">Ingresa nombre del estudiante</Label>
                            <Input id="nombre" name="nombre" onChange={this.handleChange} type="text" value={nombre} required />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="apellido">Ingresa apellido del estudiante</Label>
                            <Input id="apellido" name="apellido" onChange={this.handleChange} type="text" value={apellido} required />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="dni">Ingresa el DNI:</Label>
                            <Input id="dni" name="dni" onChange={this.handleChange} type="number" value={dni} min="0" required />
                        </FormGroup>
                        <Button color="primary">Envíar</Button>
                    </Form>
                </div>
            </div>
        );
    }
}

export default EstudiantesPendientes;

import axios from 'axios';

const myApi = axios.create({
    baseURL: 'http://127.0.0.1:3000',
    timeout: 10000,
    withCredentials: false,
    transformRequest: [(data) => JSON.stringify(data.data)],
    mode: 'no-cors',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'text/json'
    }
});


export function postStudent({ nombre, apellido, dni }) {

    return axios.get(`http://127.0.0.1:3000/api/usuario/create/${nombre}/${apellido}/${dni}`);
}

export function postMatricula(obj) {

    let data = btoa(JSON.stringify(obj));

    return axios.get(`http://127.0.0.1:3000/api/matricula/create/${data}`);
}